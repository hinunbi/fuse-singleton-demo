/**
 * Copyright 2005-2018 Red Hat, Inc.
 * <p>
 * Red Hat licenses this file to you under the Apache License, version
 * 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
package com.shb;

import com.hazelcast.config.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;

import java.util.Collection;

/**
 * A spring-boot application that includes a Camel route builder to setup the Camel routes
 */
@SpringBootApplication
@ImportResource({"classpath:spring/camel-context.xml"})
public class Application {

  private static final Logger logger = LoggerFactory.getLogger(Application.class);

  @Value("${host.ip.address}")
  private String hostIpAddress;

  @Value("${grid.name}")
  String gridName;

  @Value("${grid.password}")
  String gridPassword;

  @Value("${grid.openshift.enabled}")
  Boolean gridOpenshiftEnabled;

  // must have a main method spring-boot can run
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }


  //아래 빈을 반드시 포함합니다. {{{
  @Bean
  Config config() {

    Config config = new Config("hazelcastInstance");
    GroupConfig groupConfig = config.getGroupConfig();
    InterfacesConfig interfaces = config.getNetworkConfig().getInterfaces();
    MulticastConfig multicastConfig = config.getNetworkConfig().getJoin().getMulticastConfig();
    Collection<DiscoveryStrategyConfig> discoveryStrategyConfigs = config.getNetworkConfig().getJoin().getDiscoveryConfig().getDiscoveryStrategyConfigs();

    groupConfig.setName(gridName);
    groupConfig.setPassword(gridPassword);
    interfaces.setEnabled(true);
    interfaces.addInterface(hostIpAddress);

    if (gridOpenshiftEnabled) {
      // OpenShift
      logger.info("OpenShift clustering start...");
      multicastConfig.setEnabled(false);
      config.setProperty("hazelcast.discovery.enabled", "true");
      discoveryStrategyConfigs.add(new DiscoveryStrategyConfig("com.hazelcast.kubernetes.HazelcastKubernetesDiscoveryStrategy"));
    } else {
      // Multicast
      logger.info("Multicast clustering start...");
      multicastConfig.setEnabled(true);
      config.setProperty("hazelcast.discovery.enabled", "false");
      multicastConfig.setMulticastGroup("224.2.2.3");
      multicastConfig.setMulticastPort(54327);
    }
    return config;
  }
  // }}}
}
